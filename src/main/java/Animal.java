public abstract class Animal {
	public static void main(String[] args) {
		Animal dog = new Dog();
		Animal cat = new Cat();
		dog.say();
		cat.say();
	}
	public abstract void say();
}
